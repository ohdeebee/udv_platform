CREATE TABLE `subscribers` (
    ->   `id` int(11) NOT NULL,
    ->   `msisdn` varchar(50) NOT NULL,
    ->   `cat` varchar(50) NOT NULL,
    ->   `product_id` int(11) NOT NULL,
    ->   `date_initial_subscription` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
    ->   `date_last_updated` datetime NOT NULL,
    ->   `active` int(11) NOT NULL DEFAULT '1',
    ->   `next_expiry` datetime NOT NULL
    -> ) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `products` (
    ->   `id` int(11) NOT NULL,
    ->   `keyword` varchar(50) NOT NULL,
    ->   `name` varchar(50) NOT NULL,
    ->   `product_code` varchar(50) NOT NULL,
    ->   `service_id` varchar(50) NOT NULL
    -> ) ENGINE=InnoDB DEFAULT CHARSET=latin1;


CREATE TABLE `billing_log` (
    ->   `id` int(11) NOT NULL,
    ->   `msisdn` varchar(50) NOT NULL,
    ->   `date_billed` datetime NOT NULL,
    ->   `product_code` varchar(50) NOT NULL,
    ->   `amount` int(11) NOT NULL
    -> ) ENGINE=InnoDB DEFAULT CHARSET=latin1;
