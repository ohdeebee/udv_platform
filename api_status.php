<?php
include('config.php');
include('functions.php');

$db_conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB);

// Check connection
if (mysqli_connect_errno()) {
    die("Connection failed: " . mysqli_connect_error());
}

# check for get parameters
$msisdn = $_GET['msisdn'];
$cat = $_GET['product_code'];

if(!$msisdn || !$cat){
    $response = array('status' => 'error', 'message' => "Invalid parameters");
    echo json_encode($response);
    exit;
}

# format number
$f_msisdn = numberFormat($msisdn);

# check if record exists
$check = mysqli_fetch_array(mysqli_query($db_conn, "select count(1) as count from subscribers where active=1
and msisdn = '$f_msisdn' and product_id=(select id from products where keyword='$cat')"));

# create response
$check2 = $check['count'];

if ($check2 > 0) {
    $response = array('status' => 'active', 'message' => "Success");
}else{
    $response = array('status' => 'inactive', 'message' => "Subscriber not found");
}

# return response
echo json_encode($response);
exit;
 ?>
