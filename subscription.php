<?php
    # takes 3 parameters
    # mobile & action & product_code
    include('config.php');
    include('functions.php');

    if(!isset($_GET['mobile']) || !isset($_GET['action']) || !isset($_GET['product_code'])){
        die("Invalid parameters");
    }

    # populate parameters
    $mobile = $_GET['mobile'];
    $action = strtolower($_GET['action']);
    $product_code = $_GET['product_code'];

    # check action type
    if($action != 'sub' && $action != 'unsub'){
        die("Invalid operation");
    }

    if($action == 'sub'){

        # create timestamp
        $timestamp = getTimeStamp();

        # create auth $password
        $password = encryptPassword(SPID, SP_PASSWORD, $timestamp);

        # get appropriate url
        $url = getURL(SUBSCRIBE_URL);

        #print $url."<br/>";

        #print SPID."<br/>";

        #print SP_PASSWORD."<br/>";

        #print $password."<br/>";

        #print $timestamp."<br/>";

        # prepare xml data
        $xml_data = prepareSubscriptionXML(SPID, $password, $timestamp, $mobile, $product_code);

        # print $xml_data; exit;

        # initiate CURL
        $channel = curl_init($url);
        curl_setopt($channel, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($channel, CURLOPT_POST, 1);
        curl_setopt($channel, CURLOPT_POSTFIELDS, $xml_data);
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, 1);

        try
        {
            $output = curl_exec($channel);
            curl_close($channel);
        }
        catch (Exception $e)
        {
            echo "<h2>Exception Error!</h2>";
            echo $e->getMessage();
            die();
        }

        print_r($output);

        # TODO: Save sdp response
    }elseif($action == 'unsub'){

        # create timestamp
        $timestamp = getTimeStamp();

        # create auth $password
        $password = encryptPassword(SPID, SP_PASSWORD, $timestamp);

        # get appropriate url
        $url = getURL(SUBSCRIBE_URL);

        # prepare xml data
        $xml_data = prepareUnsubscriptionXML(SPID, $password, $timestamp, $mobile, $product_code);

        # initiate CURL
        $channel = curl_init($url);
        curl_setopt($channel, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($channel, CURLOPT_POST, 1);
        curl_setopt($channel, CURLOPT_POSTFIELDS, "$xml_data");
        curl_setopt($channel, CURLOPT_RETURNTRANSFER, 1);

        try
        {
            $output = curl_exec($channel);
            curl_close($channel);
        }
        catch (Exception $e)
        {
            echo "<h2>Exception Error!</h2>";
            echo $e->getMessage();
            die();
        }

        print_r($output);

        # TODO: Save sdp response
    }
?>
