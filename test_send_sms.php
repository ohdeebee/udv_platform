<?php

error_reporting(E_ALL); ini_set('display_errors', '1');

include('config.php');
include('functions.php');

$db_conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB);

# check for get parameters
$message = $_GET['message'];
$cat = $_GET['cat'];

if(!$message || !$cat){
    die("Invalid parameters");
}

# get product
$product = mysqli_fetch_array(mysqli_query($db_conn, "select * from products where keyword = '$cat' limit 1"));

# retrieve subscribed numbers
$result = mysqli_query($db_conn, "select * from subscribers where active = 1 and product_id = '".$product["id"]."'");

# get appropriate url
$url = getURL(SEND_SMS_URL);

  # loop through numbers
  while($row = mysqli_fetch_array($result)){
    ####### prepare parameters #####
    # create timestamp
    $timestamp = getTimeStamp();

    # create auth $password
    $password = encryptPassword(SPID, SP_PASSWORD, $timestamp);

    # prepare xml data
    $xml_data = prepareSendSmsXML(SPID, $password, $product["product_code"], $timestamp, $row["msisdn"], $message);

    # echo $xml_data;
    # exit;

    # send sms: initiate CURL
    $channel = curl_init($url);
    curl_setopt($channel, CURLOPT_HTTPHEADER, array('Content-Type: text/xml;charset=UTF-8'));
    curl_setopt($channel, CURLOPT_ENCODING, 'gzip,deflate');
    curl_setopt($channel, CURLOPT_POST, 1);
    curl_setopt($channel, CURLOPT_POSTFIELDS, "$xml_data");
    curl_setopt($channel, CURLOPT_RETURNTRANSFER, 1);

    try
    {
        $output = curl_exec($channel);
        var_dump($output);
        curl_close($channel);
    }
    catch (Exception $e)
    {
        echo "<h2>Exception Error!</h2>";
        echo $e->getMessage();
        die();
    }

  }
/*
print_r($db_conn);
exit;


$url = "http://41.206.4.162:8310/SendSmsService/services/SendSms";

$timestamp = getTimeStamp();

# create auth $password
$password = encryptPassword('2340110010661', 'Huawei123', $timestamp);

# prepare xml data
$xml_data = prepareSendSmsXML('2340110010661', $password, '23401220000026854', $timestamp, '2349060806858', "Test");

echo $xml_data;
exit;

$channel = curl_init($url);
curl_setopt($channel, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
curl_setopt($channel, CURLOPT_POST, 1);
curl_setopt($channel, CURLOPT_POSTFIELDS, "$xml_data");
curl_setopt($channel, CURLOPT_RETURNTRANSFER, 1);

try
{
    $output = curl_exec($channel);
    var_dump($output);
    curl_close($channel);
}
catch (Exception $e)
{
    echo "<h2>Exception Error!</h2>";
    echo $e->getMessage();
    die();
}
*/

?>
