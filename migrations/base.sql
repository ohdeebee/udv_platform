-- --------------------------------------------------------
-- THIS IS THE BASE FILE . The version '0'
-- --------------------------------------------------------
BEGIN;

-- create table api_response_log
CREATE TABLE `api_response_log` (
  `id` int(11) NOT NULL,
  `msisdn` varchar(50) DEFAULT NULL,
  `operation` varchar(100) NOT NULL,
  `product_code` varchar(50) DEFAULT NULL,
  `product_id` int(11) DEFAULT NULL,
  `response` text,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
);

ALTER TABLE `api_response_log`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `api_response_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;


-- create table products
CREATE TABLE `products` ( 
    `id` INT NOT NULL AUTO_INCREMENT , 
    `product_code` VARCHAR(50) NOT NULL , 
    `name` VARCHAR(50) NOT NULL , 
    `friendly_name` VARCHAR(50) NOT NULL , 
    `keyword` VARCHAR(50) NOT NULL , 
    `stop_keyword` VARCHAR(50) NOT NULL,
    `validity` INT NOT NULL , 
    `amount` INT NOT NULL , 
    `date_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
    PRIMARY KEY (`id`)
);

INSERT INTO `products`
(`product_code`, `name`, `keyword`, `friendly_name`, `stop_keyword`, `validity`, `amount`) VALUES
( 234012000023247, 'Love Talk', 'love', 'Love Talk', 'stop love', 7, 50),
( 234012000023248, 'Health Watch', 'health', 'Health Watch', 'stop health', 7, 50),
( 234012000023249, 'Banoko Games', 'games', 'Banoko Games', 'stop games', 7, 50);

-- create table subscribers
CREATE TABLE `subscribers` ( 
    `id` INT NOT NULL AUTO_INCREMENT , 
    `msisdn` VARCHAR(50) NOT NULL , 
    `product_id` INT NOT NULL , 
    `product_friendly_name` VARCHAR(50) NOT NULL , 
    `date_initial_subscription` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
    `date_last_updated` DATETIME NULL , 
    `next_expiry` DATETIME NULL , 
    `status` INT NOT NULL DEFAULT 0, 
    `date_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
    PRIMARY KEY (`id`)
);

-- create table subscription_log
CREATE TABLE `subscription_log` ( 
    `id` INT NOT NULL AUTO_INCREMENT , 
    `msisdn` VARCHAR(50) NOT NULL , 
    `operation` VARCHAR(100) NOT NULL , 
    `product_id` INT NOT NULL , 
    `product_friendly_name` VARCHAR(50) NOT NULL , 
    `date_created` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP , 
    PRIMARY KEY (`id`)
);

COMMIT;