<?php
include('../config.php');
include('../functions.php');

$db_conn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB);

// Check connection
if (mysqli_connect_errno()) {
    die("Connection failed: " . mysqli_connect_error());
}


$date = @date("Y-m-d h:i:sa");
$data = file_get_contents('php://input');

$logFile = "/var/www/html/log/mtnraw.".@date("d_m_Y").".log";
file_put_contents($logFile, "$data --$date \n", FILE_APPEND);

// echo this out so SDP know you got the message, else SDP will keeps sending
$response = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:loc="http://www.csapi.org/schema/parlayx/data/sync/v1_0/local"> <soapenv:Header/> <soapenv:Body> <loc:syncOrderRelationResponse> <loc:result>0</loc:result> <loc:resultDescription>OK</loc:resultDescription> </loc:syncOrderRelationResponse> </soapenv:Body> </soapenv:Envelope>';
echo "$response";

$myXMLData = $data;
$myXMLData2 = rtrim($myXMLData, "0");

$clean_xml = str_ireplace(['SOAP-ENV:', 'SOAP:'], '', $myXMLData2);
$doc = new DOMDocument();
$doc->loadXML($clean_xml);
$productid = $doc->getElementsByTagName('productID')->item(0)->nodeValue;
$msisdn = $doc->getElementsByTagName('ID')->item(0)->nodeValue;
$serviceid = $doc->getElementsByTagName('serviceID')->item(0)->nodeValue;
$time = $doc->getElementsByTagName('updateTime')->item(0)->nodeValue;
$keyword = $doc->getElementsByTagName('value')->item(13)->nodeValue;
$optout = $doc->getElementsByTagName('value')->item(5)->nodeValue;
$updatedesc = $doc->getElementsByTagName('updateDesc')->item(0)->nodeValue;
$updatetype = $doc->getElementsByTagName('updateType')->item(0)->nodeValue;

$newdata = "product=$productid \n msisdn=$msisdn \n serviceid=$serviceid \n time=$time \n  keyword=$keyword  \n optout=$optout \n updatedescription=$updatedesc \n updatedetype=$updatetype";
file_put_contents($logFile, "$newdata--$date \n", FILE_APPEND);

$keyword = strtolower("$keyword");
$optout = strtolower("$optout");
$num = $msisdn;

if ($updatetype == 2) {
	file_put_contents($logFile, "Step DEACTIVATION 1 \n", FILE_APPEND);
	# deactivation
	$check = mysqli_fetch_array(mysqli_query($db_conn, "select count(1) as count from subscribers where
	active=1 and msisdn = '$num' and product_id=(select id from products where product_code='$serviceid')"));
	$check2 = $check['count'];

	file_put_contents($logFile, "Step DEACTIVATION 1A: check2 = $check2 \n", FILE_APPEND);

	if ($check2 > 0) {
		mysqli_query($db_conn, "update subscribers set active=0, date_last_updated=now() where msisdn='$num' and product_id=(select id from products where product_code='$serviceid')");
		#	echo "Deactivation done";
		file_put_contents($logFile, "Step DEACTIVATION 2 \n", FILE_APPEND);
	} elseif ($check2 < 1) {
		#		echo "Not Subscribed";
	}



} elseif ($updatetype == 1) {
	file_put_contents($logFile, "Step ACTIVATION  \n", FILE_APPEND);
	# activation
	$check = mysqli_fetch_array(mysqli_query($db_conn, "select count(1) as count from subscribers
	where active=1 and msisdn = '$num' and product_id=(select id from products where product_code='$serviceid')"));

	$check2 = $check['count'];
	if ($check2 < 1) {
		mysqli_query($db_conn, "insert into subscribers (msisdn, cat, product_id, date_initial_subscription, next_expiry, active, date_last_updated)
		select '$num', keyword, id, now(), NOW() + interval 7 day,'1', now() from products where product_code='$serviceid'");

		mysqli_query($db_conn, "insert into billing_log (msisdn, description, date_billed, product_code, amount) values ('$num', 'subscription', now(),'$serviceid','50')");

		# sleep(10);

		# todo send welcome sms
		if($serviceid == '234012000023247'){
			# love talk
			$welcome = "Welcome to Love Talk! A safe place to talk about matters of the heart. To get started, use this link https://bit.ly/2WfPI0T";

		}elseif ($serviceid == '234012000023248') {
			# health watch
			$welcome = "Welcome to Health Watch! We provide health tips and information to keep you in top shape. To get started, use this link https://bit.ly/2Ht5Xob";
		}else{
			# bonako
			$welcome = "Welcome to Bonako Games Arcade! Play the best games right on your phone! To get started, use this link https://bit.ly/2MrIdQc";
		}

		# send sms
		$url = getURL(SEND_SMS_URL);

		$timestamp = getTimeStamp();

	  # create auth $password
	  $password = encryptPassword(SPID, SP_PASSWORD, $timestamp);

	  # prepare xml data
	  $xml_data = prepareSendSmsXML(SPID, $password, $serviceid, $timestamp, $num, $welcome);

	  # send sms: initiate CURL
	  $channel = curl_init($url);
    curl_setopt($channel, CURLOPT_HTTPHEADER, array('Content-Type: text/xml;charset=UTF-8'));
    curl_setopt($channel, CURLOPT_ENCODING, 'gzip,deflate');
	  curl_setopt($channel, CURLOPT_POST, 1);
	  curl_setopt($channel, CURLOPT_POSTFIELDS, "$xml_data");
	  curl_setopt($channel, CURLOPT_RETURNTRANSFER, 1);

	  try
	  {
	      $output = curl_exec($channel);
	      curl_close($channel);
	  }
	  catch (Exception $e)
	  {
	      echo "<h2>Exception Error!</h2>";
	      echo $e->getMessage();
	      die();
	  }
	} elseif ($check2 > 0) {
		#		echo "Already Subscribed";
	}



} elseif ($updatetype == 3) {
	# reactivation
	$check = mysqli_fetch_array(mysqli_query($db_conn, "select count(1) as count from subscribers where
	msisdn = '$num' and product_id=(select id from products where product_code='$serviceid')"));

	echo $check2 = $check['count'];
	if ($check2 > 0) {
		$check = mysqli_query($db_conn, "update subscribers set active=1, next_expiry=now() + interval 7 day  where msisdn = '%$num%' and product_id=(select id from products where product_code='$serviceid') limit 1");
		mysqli_query($db_conn, "insert into billing_log (msisdn, description, date_billed, product_code, amount) values ('$num', 'renewal', now(),'$serviceid','50')");

	} else {

	}

} elseif ($updatetype == 5) {
	# block subscriber
	$check = mysqli_query($db_conn, "update subscribers set active=-10  where msisdn = '$num' and product_id=(select id from products where product_code='$serviceid') limit 1");

#echo "Subscriber blocked";

} elseif ($updatetype == 6) {
	# unblock subscriber
	$check = mysqli_query($db_conn, "update subscribers set active=1  where msisdn = '$num' and product_id=(select id from products where product_code='$serviceid')");

#echo "Subscriber Unblocked";
}
