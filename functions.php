<?php
const PRODUCTS = array(
    'love' => '23401220000026852',
    'health' => '23401220000026853',
    'games' => '23401220000026854'
);


function encryptPassword($spid, $password, $timestamp){
    return md5($spid.$password.$timestamp);
}

function getTimeStamp($date=null){
    # This is not returning a UNIX timestamp
    $formatted_date = new DateTime($date);
    return $formatted_date->format('YmdHis');
}

function getProduct($key='love'){
    return PRODUCTS[$key];
}

function getURL($url){
    if(DEBUG_MODE){
        return sprintf("http://%s:%s/%s", SDP_TEST_BED, SDP_TEST_BED_PORT, $url);
    }
    return sprintf("http://%s:%s/%s", SDP_PRODUCTION, SDP_PRODUCTION_PORT, $url);
}

function prepareSubscriptionXML($spid, $password, $timestamp, $msisdn, $product){
    $xml = file_get_contents('subscriptionRequest.xml');
    # string replace
    $xml = str_replace('{SPID}', $spid, $xml);
    $xml = str_replace('{SP_PASSWORD}', $password, $xml);
    $xml = str_replace('{TIMESTAMP}', $timestamp, $xml);
    $xml = str_replace('{MSISDN}', $msisdn, $xml);
    $xml = str_replace('{PRODUCTID}', getProduct($product), $xml);

    return $xml;
}

function prepareUnsubscriptionXML($spid, $password, $timestamp, $msisdn, $product){
    $xml = file_get_contents('unsubscriptionRequest.xml');
    # string replace
    $xml = str_replace('{SPID}', $spid, $xml);
    $xml = str_replace('{SP_PASSWORD}', $password, $xml);
    $xml = str_replace('{TIMESTAMP}', $timestamp, $xml);
    $xml = str_replace('{MSISDN}', $msisdn, $xml);
    $xml = str_replace('{PRODUCTID}', getProduct($product), $xml);

    return $xml;
}

function prepareSendSmsXML($spid, $password, $serviceid, $timestamp, $msisdn, $message){
    $xml = file_get_contents('sendSmsRequest.xml');
    # $message = mb_convert_encoding($message, '8bit');

    # string replace
    $xml = str_replace('{SPID}', $spid, $xml);
    $xml = str_replace('{SP_PASSWORD}', $password, $xml);
    $xml = str_replace('{SERVICE_ID}', $serviceid, $xml);
    $xml = str_replace('{TIMESTAMP}', $timestamp, $xml);
    $xml = str_replace('{MSISDN}', $msisdn, $xml);
    $xml = str_replace('{MESSAGE}', $message, $xml);

    return $xml;
}

function numberFormat($string_n){
    $pnumber = $string_n;
    if( (substr( $string_n, 0, 4 ) === "+234") || (substr( $string_n, 0, 3 ) === "234")  || (substr( $string_n, 0, 1 ) === "0")  )
    {
        $pnumber = ltrim($pnumber, "+234");
        $pnumber = ltrim($pnumber, "234");
        $pnumber = ltrim($pnumber, "0");

        $pnumber = "234" . $pnumber;
        return $pnumber;
    }
    else
        return $string_n;

}
