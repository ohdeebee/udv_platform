<?php
    # takes 3 parameters
    # mobile & action & product_code
    include_once('config.php');
    include_once('functions.php');

    if(!isset($_GET['mobile']) || !isset($_GET['action']) || !isset($_GET['product_code'])){
        die("Invalid parameters");
    }

    # populate parameters
    $mobile = $_GET['mobile'];
    $action = strtolower($_GET['action']);
    $product_code = $_GET['product_code'];

    # check action type
    if($action != 'sub' && $action != 'unsub'){
        die("Invalid operation");
    }

    if($action == 'sub'){

        # create timestamp
        $timestamp = getTimeStamp();

        // create auth $password
        $password = encryptPassword(SPID, SP_PASSWORD, $timestamp);

        // create SOAP header
        $header_content = array(
            'spId' => SPID,
            'spPassword' => $password,
            'timeStamp' => $timestamp
        );

        $header = new SoapHeader(getUrl(SUBSCRIBE_URL), 'RequestSOAPHeader', $header_content);

        # make call to subscription SDP endpoint
        $client = new SoapClient(getUrl(SUBSCRIBE_URL), array('trace'=>1));  // The trace param will show you errors stack

        //set the Headers of Soap Client. 
        $client->__setSoapHeaders($header); 


        // web service input params
        $request_params = array(
            "userID" => array(
                'ID' => $mobile,
                'type' => 0,
            ),
            "subInfo" => array(
                'productID' => getProduct(),
                'channelID' => 'WEB',
            )
        );

        try
        {
            $response = $client->SubscribeProduct($request_params);
           //$responce_param =  $client->call("webservice_methode_name", $request_param); // Alternative way to call soap method
        }
        catch (Exception $e)
        {
            echo "<h2>Exception Error!</h2>";
            echo $e->getMessage();
        }

        // TODO: Save sdp response
    }elseif($action == 'unsub'){
        # make call to unsubscription SDP endpoint
        // TODO: Save sdp response
    }
?>
